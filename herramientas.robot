*** Settings ***
Library     Selenium2Library


*** Variables ***
${baseUrl}                     %{BASE_URL}
${os}                          %{OS}
${osVersion}                   %{OS_VERSION}
${browser}                     %{BROWSER}
${browserVersion}              %{BROWSER_VERSION}
${browserstack_userName}       %{BROWSERSTACK_USERNAME}
${browserstack_accessKey}      %{BROWSERSTACK_ACCESSKEY}


*** Keywords ***
Open Chrome
    ${remoteUrl}                Set Variable        http://${browserstack_userName}:${browserstack_accessKey}@hub.browserstack.com:80/wd/hub
    &{desiredCapabilities}      Create Dictionary   os=${os}     os_version=${osVersion}     browser=${browser}   browser_version=${browserVersion}
    Open Browser      ${baseUrl}    remote_url=${remoteUrl}     desired_capabilities=${desiredCapabilities}

    

Escribir texto en buscador
    [Arguments]             ${TEXTO_A_ESCRIBIR}
    Input text              name=q     ${TEXTO_A_ESCRIBIR}
    Execute Javascript      document.getElementsByName('btnK')[1].click()