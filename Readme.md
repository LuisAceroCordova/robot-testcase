# Instalación
---

-   Instalar python
```
sudo apt update

apt install python
```

-   Instalar pip
```
apt install python-pip
```

-   Instalar robotframework
```
pip install robotframework
```

-   Instalar selenium2library
```
pip install robotframework-selenium2library
```

# Variables de entorno

```
export BASE_URL=https://www.google.com/
export OS=windows
export OS_VERSION=10
export BROWSER=chrome
export BROWSER_VERSION=86
export BROWSERSTACK_USERNAME=changeme
export BROWSERSTACK_ACCESSKEY=changeme
```

# Uso
---

```
python -m robot testcase.robot
```